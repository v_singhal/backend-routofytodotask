package com.vbstudio.todo_task.core;

import javax.persistence.*;

/**
 * Created by vaibhavsinghal on 07/02/17.
 */
@Entity
@Table(name = "todo_task")
@NamedQueries(
        {
                @NamedQuery(
                        name = "com.vbstudio.todotask.core.TodoTask.findAll",
                        query = "SELECT t FROM TodoTask t"
                )
        }
)
public class TodoTask {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "isCompleted")
    private boolean isCompleted;

    public TodoTask() {
    }

    public TodoTask(String id, String title, String description, boolean isCompleted) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.isCompleted = isCompleted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public void setCompleted(boolean completed) {
        isCompleted = completed;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj instanceof TodoTask && id.equals(((TodoTask) obj).getId())) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "TodoTask{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", isCompleted=" + isCompleted +
                '}';
    }
}
