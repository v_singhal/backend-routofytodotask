package com.vbstudio.todo_task.db;

import com.vbstudio.todo_task.core.TodoTask;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * Created by vaibhavsinghal on 07/02/17.
 */
public class TodoTaskDao extends AbstractDAO<TodoTask> {

    /**
     * Creates a new DAO with a given session provider.
     *
     * @param sessionFactory a session provider
     */
    public TodoTaskDao(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public TodoTask findById(String id) {
        return get(id);
    }

    public TodoTask create(TodoTask todoTask) {
        return persist(todoTask);
    }

    public List<TodoTask> findAll() {
        return list(namedQuery("com.vbstudio.todotask.core.TodoTask.findAll"));
    }

    public boolean deleteTask(String taskId) {
        TodoTask todoTask = findById(taskId);
        if (todoTask != null) {
            currentSession().delete(todoTask);
            currentSession().flush();
            return true;
        }
        return false;
    }
}
