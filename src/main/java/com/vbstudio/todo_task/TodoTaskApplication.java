package com.vbstudio.todo_task;

import com.vbstudio.todo_task.core.TodoTask;
import com.vbstudio.todo_task.db.TodoTaskDao;
import com.vbstudio.todo_task.resources.TodoTaskResource;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;

import java.net.URL;
import java.net.URLClassLoader;

public class TodoTaskApplication extends Application<TodoTaskConfiguration> {
    public static void main(String[] args) throws Exception {
        // print classpath for runtime
        ClassLoader cl = TodoTaskApplication.class.getClassLoader();
        URL[] urls = ((URLClassLoader)cl).getURLs();
        for (URL url: urls) {
            System.out.println(url.getFile());
        }

        new TodoTaskApplication().run(args);
    }

    private final HibernateBundle<TodoTaskConfiguration> hibernateBundle =
            new HibernateBundle<TodoTaskConfiguration>(TodoTask.class) {
                @Override
                public DataSourceFactory getDataSourceFactory(TodoTaskConfiguration configuration) {
                    System.out.println("GETTING DATA_SOURCE_FACTORY");
                    return configuration.getDataSourceFactory();
                }
            };

    @Override
    public String getName() {
        return "todo-task";
    }

    @Override
    public void initialize(Bootstrap<TodoTaskConfiguration> bootstrap) {
        bootstrap.addBundle(new AssetsBundle());
        bootstrap.addBundle(new MigrationsBundle<TodoTaskConfiguration>() {
            @Override
            public DataSourceFactory getDataSourceFactory(TodoTaskConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }
        });
        bootstrap.addBundle(hibernateBundle);
        bootstrap.addBundle(new ViewBundle());
    }

    @Override
    public void run(TodoTaskConfiguration configuration, Environment environment) {
        final TodoTaskDao todoTaskDao = new TodoTaskDao(hibernateBundle.getSessionFactory());

        environment.jersey().register(new TodoTaskResource(todoTaskDao));
    }
}
