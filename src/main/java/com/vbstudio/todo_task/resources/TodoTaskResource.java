package com.vbstudio.todo_task.resources;

import com.codahale.metrics.annotation.Timed;
import com.vbstudio.todo_task.core.TodoTask;
import com.vbstudio.todo_task.db.TodoTaskDao;
import io.dropwizard.hibernate.UnitOfWork;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by vaibhavsinghal on 07/02/17.
 */
@Path("/task")
@Produces(MediaType.APPLICATION_JSON)
public class TodoTaskResource {
    private TodoTaskDao todoTaskDao;

    public TodoTaskResource(TodoTaskDao todoTaskDao) {
        this.todoTaskDao = todoTaskDao;
    }

    @POST
    @UnitOfWork
    @Path("/add")
    public TodoTask addTask(TodoTask todoTask) {
        return todoTaskDao.create(todoTask);
    }

    @GET
    @UnitOfWork
    @Timed
    @Path("/get")
    @Produces(MediaType.APPLICATION_JSON)
    public List<TodoTask> getAllTasks() {
        return todoTaskDao.findAll();
    }

    @DELETE
    @UnitOfWork
    @Path("/delete")
    @Produces(MediaType.APPLICATION_JSON)
    public boolean deleteTask(String taskId) {
        return todoTaskDao.deleteTask(taskId);
    }
}
