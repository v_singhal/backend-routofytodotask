##########################################################################################
# Running The Application

To test the example application run the following commands.

* To package the example run.

        mvn package

* To setup the h2 database run.

        java -jar ./target/todo_task-1.0.0-SNAPSHOT.jar db migrate todo_task.yml

* To run the server run.

        java -jar ./target/todo_task-1.0.0-SNAPSHOT.jar server todo_task.yml


# Using The Application

* To POST data into the application.

	`curl -H "Content-Type: application/json" -X POST -d '{"title":"Task Title","description":"Task Details", "isCompleted":"true/false"}' http://localhost:8080/task/add`
	
* To GET data from the application.

	`curl -H "Content-Type: application/json" -X GET http://localhost:8080/task/get`
	
* To DELETE data from the application.

	`curl -H "Content-Type: application/json" -X DELETE -d 'task_id' http://localhost:8080/task/delete`